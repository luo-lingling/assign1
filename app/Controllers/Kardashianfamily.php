<?php namespace App\Controllers;
 class Kardashianfamily extends \CodeIgniter\Controller {

     public function getIndex() {
     
     // connect to the model
    $family = new \App\Models\family();
     // retrieve all the records
    $records = $family->findAll();

    
   // get a template parser
    $parser = \Config\Services::parser();
   // tell it about the substitions
    return $parser->setData(['records' => $records])
   // and have it render the template with those
                    ->render('familylist');
 }
 
 public function getshowme($id){
     // connect to the model
     $family= new \App\Models\family();
 // retrieve all the records
     $record = $family->find($id);
 // get a template parser
     $parser = \Config\Services::parser();
 // tell it about the substitions
     return $parser->setData($record)
 // and have it render the template with those
                     ->render('oneactor');
 }
 }
